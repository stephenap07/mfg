#ifndef ACTOR_H_
#define ACTOR_H_

#include <array>

constexpr std::size_t InvalidId = std::numeric_limits<std::size_t>::max();
using Id = std::size_t;

struct Game;

struct Attributes
{
   int strength;
   int constitution;
   int dexterity;
   int intelligence;
};

enum ActionType : uint16_t
{
   MoveLeft,
   MoveRight,
   MoveUp,
   MoveDown,
   Attack,
   Die
};

struct Action
{
   ActionType type;
   Id owner;
   Id target;
};

struct ActorState
{
   int tileX = 0;
   int tileY = 0;

   float x = 0.f;
   float y = 0.f;

   int xDir = 0;
   int yDir = 0;

   uint64_t timerX = 0;
   uint64_t timerY = 0;

   uint32_t highlightColor;
};

struct Actor
{
   enum Direction : uint8_t
   {
      North,
      South,
      East,
      West
   };

   Id id = InvalidId;

   // Components
   Id weaponHolderId = InvalidId;
   Id mortalId = InvalidId;

   int type = 0;
   int tileX = 0;
   int tileY = 0;
   float x = 0.f;
   float y = 0.f;
   std::string name;

   Direction faceDirection = Direction::East;
   ActorState actorState;
   std::unique_ptr<Action> nextAction;
};

void tickMob(Actor* actor, Game* game);
void doAction(Action* action, Game* game);
void drawActor(Actor* actor, ActorState* state);

void setNextAction(Actor* actor, ActionType actionType);
void actorAttack(Actor* actor, uint16_t targetActorId);

int solveMove(int* dir, uint64_t* timer, float* pos, int* tilePos);

template <typename T>
struct ComponentRegister
{
   using Type = T;
   static constexpr Id MaxId = InvalidId;
   std::size_t playerId = 0;
   std::size_t lastId = 0;
   std::array<std::unique_ptr<T>, 128> components;
   std::vector<std::size_t> activeComponents;
};

using ActorRegister = ComponentRegister<Actor>;

bool isValidId(std::size_t id)
{
   return id != InvalidId;
}

template <typename R>
auto add(R* reg)
{
   using Type = typename R::Type;
   reg->components[reg->lastId] = std::make_unique<Type>();
   Type* comp = reg->components[reg->lastId].get();
   reg->activeComponents.push_back(reg->lastId);
   ++reg->lastId;
   return std::make_tuple(reg->lastId - 1, comp);
}

template <typename R>
typename R::Type* get(R* reg, std::size_t id)
{
   if (id < reg->components.size())
   {
      return reg->components[id].get();
   }
   return nullptr;
}

template <typename R>
void clear(R* reg)
{
   for (auto& a : reg->components)
   {
      a.release();
   }

   reg->activeComponents.clear();
   reg->lastId = 0;
}

template <typename R>
bool isValidId(R* reg, Id id)
{
   return id < reg->components.size();
}

#endif

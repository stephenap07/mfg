#include "actor.h"
#include "bsp.h"
#include "level.h"
#include "mortal.h"
#include "player.h"
#include "weapon.h"

#include <array>
#include <deque>
#include <list>
#include <memory>

enum GameState
{
   StartGame,
   GameOver,
   Idle,
   NewTurn
};

struct Game
{
   Clock gameClock;
   Level level;
   Player player;
   int ActionCount;
   bool newTurn = false;
   GameState state = StartGame;
   ActorRegister actorRegister;
   WeaponHolderRegister weaponHolderRegister;
   MortalRegister mortalRegister;

   std::vector<std::unique_ptr<Action>> turnActions;
   std::vector<std::vector<std::unique_ptr<Action>>> allActions;
};

struct GuiState
{
   bool mouseDown = false;
   bool actionWindowToggle = false;

   int mouseX = 0;
   int mouseY = 0;
   int mouseDownX = 0;
   int mouseDownY = 0;
};


void initGame();
std::size_t getActorFromLevel(Game* game, int newX, int newY);
void initObjectPositions(Game* game);
void tickWorld(Game* game);
void executeActions(Game* game);
void drawWorld(Game* game);

bool isMouseOver(GuiState* state, int x0, int y0, int x1, int y1);
void doActionWindow(GuiState* state);
void doHudGui(GuiState* state, Game* game);
void doGameOverGui(GuiState* state, Game* game);

//	Variables
static Game game;
constexpr auto GameScreenWidth = ScaledWidth - 95;
constexpr auto GameScreenHeight = ScaledHeight - 60;

void gameLoop()
{
   bool quit = false;
   bool editorEnabled = false;
   const Uint8* state = nullptr;

   GuiState guiState;

   while (!quit)
   {
      game.gameClock.tick();

      while (SDL_PollEvent(&event))
      {
         switch (event.type)
         {
         case SDL_MOUSEWHEEL:
         case SDL_MOUSEMOTION:
         case SDL_MOUSEBUTTONDOWN:
         case SDL_MOUSEBUTTONUP:
            if (editorEnabled)
            {
               stbte_mouse_sdl(game.level.stbTileMap, &event, ScaledWidth / ScreenWidth, ScaledHeight / ScreenHeight, 0, 0);
            }
            break;
         };

         switch (event.type)
         {
         case SDL_QUIT:
            quit = true;
            break;
         case SDL_MOUSEMOTION:
            guiState.mouseX = event.motion.x / RatioW;
            guiState.mouseY = event.motion.y / RatioH;
            break;
         case SDL_MOUSEBUTTONDOWN:
            if (event.button.button == SDL_BUTTON_RIGHT)
            {
               guiState.mouseDownX = event.button.x / RatioW;
               guiState.mouseDownY = event.button.y / RatioH;
               guiState.mouseDown = true;
            }
            break;
         case SDL_MOUSEBUTTONUP:
            if (event.button.button == SDL_BUTTON_RIGHT)
            {
               guiState.mouseDown = false;
            }
            break;
         case SDL_KEYDOWN:
            state = SDL_GetKeyboardState(nullptr);

            if (state[SDL_SCANCODE_LCTRL] && state[SDL_SCANCODE_E])
            {
               // toggle the editor
               editorEnabled = !editorEnabled;
               if (!editorEnabled)
               {
                  initObjectPositions(&game);
               }
            }

            if (state[SDL_SCANCODE_LGUI] && state[SDL_SCANCODE_S])
            {
               printf("Writing level\n");
               writeLevel(&game.level, "assets/test.map");
            }

            if (state[SDL_SCANCODE_LGUI] && state[SDL_SCANCODE_O])
            {
               printf("Reading level\n");
               readLevel(&game.level, "assets/test.map");
               initObjectPositions(&game);
            }

            if (!editorEnabled)
            {
               handlePlayerEvents(game.player.actor, &game.player.actor->actorState, &game, state);
            }

            break;
         };
      }

      state = SDL_GetKeyboardState(nullptr);
      if (state[SDL_SCANCODE_W] && state[SDL_SCANCODE_LGUI])
      {
         quit = true;
      }

      glClear(GL_COLOR_BUFFER_BIT);

      if (!editorEnabled)
      {
         if (game.state != GameOver)
         {
            tickWorld(&game);
            drawWorld(&game);

            doActionWindow(&guiState);
            doHudGui(&guiState, &game);
         }
         else
         {
            drawWorld(&game);
            doGameOverGui(&guiState, &game);
            if (state[SDL_SCANCODE_R])
            {
               initGame();
               game.state = StartGame;
            }
         }
      }
      else
      {
         stbte_tick(game.level.stbTileMap, game.gameClock.delta);

         beginSpriteInstance();
         stbte_draw(game.level.stbTileMap);
         endSpriteInstance();
      }

      SDL_GL_SwapWindow(window);

      if (game.gameClock.delta < 1.f / FramesPerSecond)       
      {
         SDL_Delay((1.f / FramesPerSecond) - game.gameClock.delta);
      }
   }
}

void initGame()
{
   game.allActions.clear();
   clear(&game.actorRegister);
   clear(&game.weaponHolderRegister);
   clear(&game.mortalRegister);
   initLevel(&game.level);
   //readLevel(&game.level, "assets/test.map");
   generateDungeon(&game.level);
   initObjectPositions(&game);
}

std::size_t getActorFromLevel(Game* game, int newX, int newY)
{
   for (auto actorId : game->actorRegister.activeComponents)
   {
      Actor* actor = get(&game->actorRegister, actorId);
      if (actor->tileX == newX && actor->tileY == newY)
      {
         return actorId;
      }
   }

   return std::numeric_limits<std::size_t>::max();
}

void initObjectPositions(Game* game)
{
   clear(&game->actorRegister);
   clear(&game->weaponHolderRegister);

   Level* level = &game->level;

   for (int mapy = 0; mapy < level->height; ++mapy)
   {
      for (int mapx = 0; mapx < level->width; ++mapx)
      {
         for (int l = 0; l < level->maxLayers; ++l)
         {
            if (isActor(&game->level, mapx, mapy, l))
            {
               std::size_t actorId;
               Actor* actor;
               std::tie(actorId, actor) = add(&game->actorRegister);
               actor->id = actorId;
               actor->type = getTileType(level, mapx, mapy, l);
               actor->tileX = mapx;
               actor->tileY = mapy;

               std::size_t mortalId;
               Mortal* mortal;
               std::tie(mortalId, mortal) = add(&game->mortalRegister);
               mortal->actorId = actorId;
               actor->mortalId = mortalId;
               if (isMobType(actor->type))
               {
                  mortal->hp = 10;
               }

               // Check if the actor is the player
               if (actor->type == 234)
               {
                  actor->name = "Player";
                  std::size_t weaponHolderId;
                  WeaponHolder* weaponHolder;
                  std::tie(weaponHolderId, weaponHolder) = add(&game->weaponHolderRegister);
                  weaponHolder->actorId = actorId;
                  weaponHolder->weaponType = WeaponType::Sword;
                  actor->weaponHolderId = weaponHolderId;

                  game->player.actor = actor;
                  initPlayer(&game->player);
               }
               else if (isMobType(actor->type))
               {
                  actor->name = "Mob";
               }
            }
         }
      }
   }
}

void tickWorld(Game* game)
{
   if (game->state == GameOver)
   {
      return;
   }

   // Update actors
   if (tickPlayer(game->player.actor, game))
   {
      game->state = NewTurn; 
      ++game->ActionCount;
      game->turnActions.emplace_back(std::move(game->player.actor->nextAction));
      game->player.actor->nextAction = nullptr;
   }

   for (auto a : game->actorRegister.activeComponents)
   {
      Actor* actor = game->actorRegister.components[a].get();
      if (actor != game->player.actor)
      {
         tickMob(actor, game);
         if (actor->nextAction)
         {
            game->turnActions.emplace_back(std::move(actor->nextAction));
            actor->nextAction = nullptr;
         }
      }
   }

   for (auto a : game->mortalRegister.activeComponents)
   {
      Mortal* mortal = game->mortalRegister.components[a].get();
      tickMortal(mortal, game);
   }

   executeActions(game);

   auto mortalPlayer = get(&game->mortalRegister, game->player.actor->mortalId);
   if (!isAlive(mortalPlayer))
   {
      game->state = GameOver;
   }
   else
   {
      game->state = Idle;
   }
}

void executeActions(Game* game)
{
   for (auto a : game->actorRegister.activeComponents)
   {
      for (auto& action : game->turnActions)
      {
         doAction(action.get(), game);
      }

      game->allActions.emplace_back(std::move(game->turnActions));
      game->turnActions.clear();
   }
}

void drawWorld(Game* game)
{
   beginSpriteInstance();
   setView(game->player.actor->x - GameScreenWidth / 2, game->player.actor->y - GameScreenHeight / 2);
   for (int l = 0; l < game->level.maxLayers; ++l)
   {
      drawLevel(&game->level, l, getViewX(), getViewY(), GameScreenWidth, GameScreenHeight);

      if (l == 2)
      {
         for (auto a : game->actorRegister.activeComponents)
         {
            Actor* actor = game->actorRegister.components[a].get();
            // Draw actors
            if ((actor->x + SpacingX) >= getViewX() &&
                actor->x <= (getViewX() + GameScreenWidth) &&
                (actor->y + SpacingY) >= getViewY() &&
                actor->y <= (getViewY() + GameScreenHeight))
            {
               drawActor(actor, &actor->actorState);
            }
         }

         drawWeaponHolders(game);
      }
   }

   //drawRectDeferred(game->player.actor->x, game->player.actor->y, game->player.actor->x + SpacingX, game->player.actor->y + SpacingY, 0xBBA9A9A9);

   endSpriteInstance();
   resetView();
}

bool isMouseOver(GuiState* state, int x0, int y0, int x1, int y1)
{
   if ((state->mouseX >= x0 && state->mouseX <= x1) && (state->mouseY >= y0 && state->mouseY <= y1))
   {
      return true;
   }
   return false;
}

void doActionWindow(GuiState* state)
{
   const int w = 50;
   const int h = 80;
   if (state->mouseDown || state->actionWindowToggle)
   {
      state->actionWindowToggle = true;
      int x = state->mouseDownX;
      int y = state->mouseDownY;
      if (isMouseOver(state, x, y, x + w, y + h))
      {
         drawRect(x, y, x + w, y + h, 0xBBA9A9A9);
      }
      else
      {
         state->actionWindowToggle = false;
      }
   }
}

void doHudGui(GuiState* state, Game* game)
{
   drawBorderedRect(0, ScaledHeight - 80, ScaledWidth, ScaledHeight, 2, 0xFFFCEA76, 0xFF7F763B);
   drawBorderedRect(ScaledWidth - 100, 0, ScaledWidth, ScaledHeight - 80 + 1, 2, 0xFFFCEA76, 0xFF7F763B);

   if (0)
   {
      drawText(10, ScaledHeight - 60, "Tile X: " + std::to_string(game->player.actor->tileX));
      drawText(10, ScaledHeight - 42, "Tile Y: " + std::to_string(game->player.actor->tileY));
      drawText(80, ScaledHeight - 60, "X: " + std::to_string(game->player.actor->x));
      drawText(80, ScaledHeight - 42, "Y: " + std::to_string(game->player.actor->y));
   }

   auto room = game->level.bsp->findNode(game->player.actor->tileX, game->player.actor->tileY);
   if (room)
   {
      if (room->isLeaf())
      {
         drawText(80, ScaledHeight - 22, "Room: " + std::to_string(room->index));
      }
      else
      {
         drawText(80, ScaledHeight - 22, "Corridor: " + std::to_string(room->index));
      }
   }

   {
      drawText(ScaledWidth - 95, 20, std::string("Strength: ") + std::to_string(game->player.attributes.strength));
      drawText(ScaledWidth - 95, 35, std::string("Constitution: ") + std::to_string(game->player.attributes.constitution));
      drawText(ScaledWidth - 95, 50, std::string("Dexterity: ") + std::to_string(game->player.attributes.dexterity));
      drawText(ScaledWidth - 95, 65, std::string("Intelligence: ") + std::to_string(game->player.attributes.intelligence));
      drawText(ScaledWidth - 95, 80, std::string("HP: ") + std::to_string(get(&game->mortalRegister, game->player.mortalId)->hp));
   }

   drawSprite(ScaledWidth - 95, 120, ScaledWidth - 95 + 88, 120 + 44, 259, 10, 44, 22);

   // Event log
   glEnable(GL_SCISSOR_TEST);
   glScissor(0, 0, ScreenWidth, 78 * RatioH);
   int height = 10;
   for (auto rItr = game->allActions.rbegin(); rItr != game->allActions.rend(); ++rItr)
   {
      for (auto aItr = rItr->rbegin(); aItr != rItr->rend(); ++aItr)
      {
         if ((*aItr)->type == ActionType::Attack)
         {
            auto owner = get(&game->actorRegister, (*aItr)->owner);
            auto target = get(&game->actorRegister, (*aItr)->target);
            drawText(200, ScaledHeight - height, owner->name + std::string(" attacks ") + target->name);
            height += 15;
         }
      }

      if (height > 80)
      {
         break;
      }
   }
   glDisable(GL_SCISSOR_TEST);

   // Inventory
   //drawRect(ScaledWidth - 92, 150, ScaledWidth - 92 + 88, 150 + 44, );
}

bool doButton(GuiState* state, int* x, int* y, const char* text)
{
   auto width = 70;
   auto height = 20;

   drawRect(*x, *y, *x + width, *y + height, 0xFFA9A9A9);
   drawText(*x, *y + height / 2, text);

   *y += 20 + 5;

   return false;
}

void doGameOverGui(GuiState* state, Game* game)
{
   auto width = 100;
   auto height = 30;

   auto x = ScaledWidth / 2 - width * 2;
   auto y = ScaledHeight / 2 - height * 2;

   drawRect(x, y, x + width, y + height, 0xFFA9A9A9);
   drawText(x + width / 4 - 10, y + height / 2, "Game Over");

   int buttonX = x + width / 8;
   int buttonY = y + height + 10;

   doButton(state, &buttonX, &buttonY, "Restart (R)");
   doButton(state, &buttonX, &buttonY, "Quit (Q)");
}

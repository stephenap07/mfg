#ifndef BSP_H_
#define BSP_H_

#include <functional>
#include <memory>

struct BspTree
{
   using BspPtr = std::unique_ptr<BspTree>;

   int x;
   int y;
   int width;
   int height;

   int cellX;
   int cellY;
   int cellWidth;
   int cellHeight;

   int index;

   BspTree* parent;
   BspPtr front;
   BspPtr back;

   BspTree(int x, int y, int w, int h, BspTree* parent = nullptr);

   void splitRecursive(int numLevels, int minW, int minH, float wRatio, float hRatio);
   void split(bool horizontal, int value);
   void visit(std::function<bool(BspTree&)> visitor);
   BspTree* findNode(int x, int y);
   bool isLeaf() const;
};

#endif

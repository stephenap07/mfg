CXX=clang++
CXXFLAGS=-std=c++1z -arch x86_64
LIBS=-framework OpenGL -lSDL2 -lGLEW

all:
	$(CXX) $(CXXFLAGS) sdl_main.cpp -o hleg $(LIBS)

opt: CXXFLAGS += -O3
opt:
	$(CXX) $(CXXFLAGS) sdl_main.cpp -o hleg $(LIBS)

debug: CXXFLAGS += -DDEBUG -g -fsanitize=address
debug:
	$(CXX) $(CXXFLAGS) sdl_main.cpp -o hleg $(LIBS)

clean:
	rm hleg

#include "player.h"

#include "actor.h"


void initPlayer(Player* player)
{
   player->attributes.strength = 3;
   player->attributes.constitution = 3;
   player->attributes.dexterity = 3;
   player->attributes.intelligence = 3;
}

// Player methods
void handlePlayerEvents(Actor* actor, ActorState* actorState, Game* game, const Uint8* state)
{
   if (state[SDL_SCANCODE_RIGHT])
   {
      actor->actorState.xDir = 1;
   }
   else if (state[SDL_SCANCODE_LEFT])
   {
      actor->actorState.xDir = -1;
   }

   if (state[SDL_SCANCODE_UP])
   {
      actor->actorState.yDir = -1;
   }
   else if (state[SDL_SCANCODE_DOWN])
   {
      actor->actorState.yDir = 1;
   }
}

bool tickPlayer(Actor* actor, Game* game)
{
   if (game->newTurn)
   {
      return false;
   }

   if (actor->nextAction &&
       actor->nextAction->type == ActionType::Die)
   {
      return true;
   }

   int newX = actor->tileX + actor->actorState.xDir;
   int newY = actor->tileY + actor->actorState.yDir;

   if ((newX != actor->tileX || newY != actor->tileY) &&
       !canMoveTo(game, newX, newY))
   {
      auto targetActorId = getActorFromLevel(game, newX, newY);

      if (isValidId(&game->actorRegister, targetActorId))
      {
         actorAttack(actor, targetActorId);
      }

      actor->actorState.xDir = 0;
      actor->actorState.yDir = 0;
   }

   auto xDir = solveMove(&actor->actorState.xDir, &actor->actorState.timerX, &actor->actorState.x, &actor->tileX);
   auto yDir = solveMove(&actor->actorState.yDir, &actor->actorState.timerY, &actor->actorState.y, &actor->tileY);

   actor->x = actor->tileX * SpacingX + actor->actorState.x;
   actor->y = actor->tileY * SpacingY + actor->actorState.y;

   if (xDir)
   {
      setNextAction(actor, xDir > 0 ? ActionType::MoveRight : ActionType::MoveLeft);
   }

   if (yDir)
   {
      setNextAction(actor, yDir > 0 ? ActionType::MoveDown : ActionType::MoveUp);
   }

   return actor->nextAction != nullptr;
}

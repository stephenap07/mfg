#include "weapon.h"

static constexpr std::array<Weapon, 2> Weapons = {
   Weapon{}, // hand
   Weapon{},  // sword
};

void tickWeapon(Game* game)
{
   for (auto w : game->weaponHolderRegister.activeComponents)
   {
      auto weaponHolder = get(&game->weaponHolderRegister, w);
      Actor* actor = get(&game->actorRegister, weaponHolder->actorId);

      if (weaponHolder->startAttack)
      {
         weaponHolder->angle = Weapons[weaponHolder->weaponType].startAngle;

         weaponHolder->attackInProgress = true;
         weaponHolder->startAttack = false;
      }
   }
}

void drawWeaponHolders(Game* game)
{
   for (auto w : game->weaponHolderRegister.activeComponents)
   {
      auto weaponHolder = get(&game->weaponHolderRegister, w);
      Actor* actor = get(&game->actorRegister, weaponHolder->actorId);
      auto x = actor->x + weaponHolder->hand.startX;
      auto y = actor->y + weaponHolder->hand.startY;

      auto transform = glm::rotate(glm::mat4(), weaponHolder->angle, glm::vec3(0, 0, 1));
      transform = glm::translate(transform, glm::vec3(weaponHolder->hand.radius, weaponHolder->hand.radius, 0));

      if (weaponHolder->weaponType == WeaponType::Sword)
      {
         auto transform = glm::rotate(glm::mat4(), weaponHolder->angle, glm::vec3(0, 0, 1));
         transform = glm::translate(transform, glm::vec3(-2, -16, 0));
         transform = glm::translate(transform, glm::vec3(weaponHolder->hand.radius, weaponHolder->hand.radius, 0));
         drawSpriteInstanced(x, y, x + 7, y + 20, 116, 42, 7, 20, transform);
      }

      // Draw hand square
      drawSpriteInstanced(x, y, x + 3, y + 3, 81, 134, 3, 3, transform);
   }
}

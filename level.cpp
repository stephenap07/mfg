#include "level.h"

#include "bsp.h"

#include <cctype>
#include <random>

// Private stuff

static std::array<uint16_t, 17> walls = { 0, 1, 2, 3, 4, 7, 8, 16, 17, 18, 19, 20, 23, 24, 52, 68, 69 };
static std::array<uint16_t, 18> floors = {
   // base wall
   // left, middle, right, slime
   32,      33,     34,    35,
   48, 49, 50, 51,
   66,
   // middle section
   96, 97, 98,
   99, 112, 113, 114, 116, 117
};

static std::array<uint16_t, 7> actors = {
   234, // Player
   208, // Baby stone
   209, // Baby water
   210, // Baby flame
   211, // Baby wind
   212, // Baby grass
   213, // Baby blob
};

static std::array<uint16_t, 6> babyMobs = {
   208, // Baby stone
   209, // Baby water
   210, // Baby flame
   211, // Baby wind
   212, // Baby grass
   213, // Baby blob
};

static void initStbTileMap(Level* level);
static void writeLevelToFile(Level* level, std::string filename);
static void readLevelFromFile(Level* level, std::string filename);

static void convertStbTileMapToLevel(Level* level);
static void convertLevelToStbTilemap(Level* level);

template<std::size_t Size>
uint16_t randomFromArray(std::array<uint16_t, Size>& arr)
{
   static std::random_device rd;
   static std::mt19937 gen(rd());
   std::uniform_int_distribution<> dis(0, Size - 1);
   return arr[dis(gen)];
}

constexpr auto bspW = 100;
constexpr auto bspH = 100;

void initLevel(Level* level)
{
   level->bsp.reset(new BspTree(0, 0, bspW, bspH));
   initStbTileMap(&game.level);
}

void writeLevel(Level* level, const std::string& filename)
{
   convertLevelToStbTilemap(level);
   writeLevelToFile(level, filename);
}

void readLevel(Level* level, const std::string& filename)
{
   readLevelFromFile(level, filename);
   convertLevelToStbTilemap(level);
}

uint16_t getTileType(Level* level, int x, int y, int l)
{
   const auto area = level->width * level->height;
   return level->data[area * l + y * level->width + x];
}

void setTile(Level* level, uint16_t value, int x, int y, int l)
{
   const auto area = level->width * level->height;
   level->data[area * l + y * level->width + x] = value;
}

bool isEmpty(Level* level, int x, int y, int l)
{
   return std::numeric_limits<uint16_t>::max() == getTileType(level, x, y, l);
}

bool isActor(Level* level, int x, int y, int l)
{
   auto id = getTileType(level, x, y, l);
   return std::find(actors.begin(), actors.end(), id) != actors.end();
}

bool isWall(Level* level, int x, int y)
{
   auto id = getTileType(level, x, y, 0);
   if (id == std::numeric_limits<decltype(id)>::max()
       || std::find(walls.begin(), walls.end(), id) != walls.end())
   {
      return true;
   }
   return false;
}

bool isFloor(Level* level, int x, int y)
{
   auto id = getTileType(level, x, y, 0);
   if (std::find(floors.begin(), floors.end(), id) != floors.end())
   {
      return true;
   }
   return false;
}

bool isMobType(std::size_t id)
{
   return std::find(babyMobs.begin(), babyMobs.end(), id) != babyMobs.end();
}

void drawLevel(Level* level, int layer, int screenX, int screenY, int screenWidth, int screenHeight)
{
   int maxX = 0;
   int maxY = 0;
   for (int mapy = 0; mapy < level->height; ++mapy)
   {
      for (int mapx = 0; mapx < level->width; ++mapx)
      {
         auto x = mapx * level->spacingX;
         auto y = mapy * level->spacingY;

         if ((x + SpacingX) >= screenX &&
             x <= (screenX + screenWidth) &&
             (y + SpacingY) >= screenY &&
             y <= (screenY + screenHeight))
         {
            auto id = getTileType(level, mapx, mapy, layer);
            if (id != std::numeric_limits<decltype(id)>::max()
                && !isActor(level, mapx, mapy, layer))
            {
               drawSpriteInstanced(x, y,
                                   x + level->spacingX, y + level->spacingY,
                                   (id % level->spacingX) * level->spacingX, (id / level->spacingY) * level->spacingY,
                                   level->spacingX, level->spacingY);
            }
         }
      }
   }
}

void initStbTileMap(Level* level)
{
   const std::size_t maxTiles = walls.size() + floors.size() + actors.size();

   level->stbTileMap = stbte_create_map(level->width, level->height, level->maxLayers, level->spacingX, level->spacingY, maxTiles);

   if (!level->stbTileMap)
   {
      fprintf(stderr, "Error initializing tile map\n");
      return;
   }

   for (auto i : walls)
   {
      stbte_define_tile(level->stbTileMap, i, 0xFF, "Walls");
   }

   for (auto i : floors)
   {
      stbte_define_tile(level->stbTileMap, i, 0xFF, "Floors");
   }

   for (auto i : actors)
   {
      stbte_define_tile(level->stbTileMap, i, 4, "Actors");
   }

   stbte_set_display(0, 0, ScaledWidth, ScaledHeight);
}

struct Lexer
{
   std::vector<char> buffer;
   std::vector<char>::iterator cursor;
   std::vector<std::string> tokens;
};

void initializeLevel(Level* level)
{
}

void writeLevelToFile(Level* level, std::string filename)
{
   FILE* fp = nullptr;
   fp = fopen(filename.c_str(), "w");
   if (!fp)
   {
      fprintf(stderr, "Can't open %s\n", filename.c_str());
   }

   fprintf(fp, "width = %lu\n", level->width);
   fprintf(fp, "height = %lu\n", level->height);
   fprintf(fp, "spacingX = %i\n", level->spacingX);
   fprintf(fp, "spacingY = %i\n", level->spacingY);
   fprintf(fp, "maxLayers = %i\n", level->maxLayers);

   for (int l = 0; l < 5; ++l)
   {
      fprintf(fp, "Layer %i =\n", l);
      for (int y = 0; y < level->height; ++y)
      {
         for (int x = 0; x < level->width; ++x)
         {
            fprintf(fp, "%i", level->data[y * level->height + x]);
            if (x != (level->width - 1) || y != (level->height - 1))
            {
               fprintf(fp, ",");
            }
         }
         fprintf(fp, "\n");
      }
      fprintf(fp, "\n");
   }

   fclose(fp);
}

inline char nextChar(Lexer* lexer)
{
   if (++lexer->cursor == lexer->buffer.end())
   {
      return EOF;
   }
   return *lexer->cursor;
}

inline char skipSpace(Lexer* lexer)
{
   if (lexer->cursor == lexer->buffer.end())
   {
      return EOF;
   }

   char lastChar = *lexer->cursor;

   // skip spaces
   while (std::isspace(lastChar) || lastChar == '\n')
   {
      lastChar = nextChar(lexer);
   }
   return lastChar;
}

void readLevelFromFile(Level* level, std::string filename)
{
   level->data.clear();

   FILE* fp = nullptr;
   fp = fopen(filename.c_str(), "r");
   if (!fp)
   {
      fprintf(stderr, "Can't open %s\n", filename.c_str());
   }

   std::size_t size = 0;
   fseek(fp, 0, SEEK_END);
   size = ftell(fp);
   fseek(fp, 0, SEEK_SET);

   if (size == 0)
   {
      fclose(fp);
      return;
   }

   Lexer lexer;
   lexer.buffer.resize(size);
   fread(&lexer.buffer[0], 1, size, fp);
   lexer.cursor = lexer.buffer.begin();

   std::vector<std::string> tokens;

   char lastChar = *lexer.cursor;
   lastChar = skipSpace(&lexer);

   // ID / Reserved words
   if (std::isalpha(lastChar))
   {
      while (lastChar != EOF)
      {
         std::string tokenStr;
         tokenStr += lastChar;

         while (std::isalnum(lastChar = nextChar(&lexer)))
         {
            tokenStr += lastChar;
         }

         if (tokenStr == "width"
             || tokenStr == "height"
             || tokenStr == "spacingX"
             || tokenStr == "spacingY"
             || tokenStr == "maxLayers")
         {
            lastChar = skipSpace(&lexer);
            if (lastChar == '=')
            {
               lastChar = nextChar(&lexer);
               lastChar = skipSpace(&lexer);
               std::string digitStr;
               while (std::isdigit(lastChar))
               {
                  digitStr += lastChar;
                  lastChar = nextChar(&lexer);
               }
               int digitVal = std::stoi(digitStr);
               if (tokenStr == "width")
               {
                  level->width = digitVal;
               }
               else if (tokenStr == "height")
               {
                  level->height = digitVal;
               }
               else if (tokenStr == "spacingX")
               {
                  level->spacingX = digitVal;
               }
               else if (tokenStr == "spacingY")
               {
                  level->spacingY = digitVal;
               }
               else if (tokenStr == "maxLayers")
               {
                  level->maxLayers = digitVal;
               }
            }
            else
            {
               printf("Error: Expected '='\n");
               break;
            }
         }
         else if (tokenStr == "layer" || tokenStr == "Layer")
         {
            lastChar = nextChar(&lexer);
            lastChar = skipSpace(&lexer);
            std::string digitStr;

            int layerN = 0;
            while (std::isdigit(lastChar))
            {
               digitStr += lastChar;
               lastChar = nextChar(&lexer);
            }

            if (!digitStr.empty())
            {
               layerN = std::stoi(digitStr);
            }

            lastChar = skipSpace(&lexer);

            if (lastChar == '=')
            {
               lastChar = nextChar(&lexer);
               lastChar = skipSpace(&lexer);
               std::string firstDigitStr;
               if (lastChar == '-')
               {
                  firstDigitStr += lastChar;
                  lastChar = nextChar(&lexer);
               }
               while (std::isdigit(lastChar))
               {
                  firstDigitStr += lastChar;
                  lastChar = nextChar(&lexer);
               }

               int x = 0;
               int y = 0;
               if (!firstDigitStr.empty())
               {
                  if (level->data.empty())
                  {
                     level->data.resize(level->width * level->height * level->maxLayers);
                     std::fill(level->data.begin(), level->data.end(), std::numeric_limits<uint16_t>::max());
                  }

                  auto area = level->width * level->height;
                  level->data[area * layerN + y * level->width + x] = std::stoi(firstDigitStr);
               }

               lastChar = skipSpace(&lexer);

               while (lastChar == ',')
               {
                  ++x;
                  if (x > (level->width - 1))
                  {
                     ++y;
                     x = 0;
                  }

                  std::string digitStr;

                  lastChar = nextChar(&lexer);
                  lastChar = skipSpace(&lexer);

                  if (lastChar == '-')
                  {
                     digitStr += lastChar;
                     lastChar = nextChar(&lexer);
                  }

                  while (std::isdigit(lastChar))
                  {
                     digitStr += lastChar;
                     lastChar = nextChar(&lexer);
                  }

                  if (!digitStr.empty())
                  {
                     auto area = level->width * level->height;
                     level->data[area * layerN + y * level->width + x] = std::stoi(digitStr);
                  }

                  lastChar = skipSpace(&lexer);
               }
            }
         }

         lastChar = skipSpace(&lexer);
      }
   }

   fclose(fp);
}

void convertLevelToStbTilemap(Level* level)
{
   stbte_set_dimensions(level->stbTileMap, level->width, level->height);

   for (int y = 0; y < level->height; ++y)
   {
      for (int x = 0; x < level->width; ++x)
      {
         for (int l = 0; l < level->maxLayers; ++l)
         {
            stbte_set_tile(level->stbTileMap, x, y, l, getTileType(level, x, y, l));
         }
      }
   }
}

void convertStbTileMapToLevel(Level* level)
{
   int w = 0;
   int h = 0;
   stbte_get_dimensions(level->stbTileMap, &w, &h);
   level->width = w;
   level->height = h;

   for (int y = 0; y < level->height; ++y)
   {
      for (int x = 0; x < level->width; ++x)
      {
         short* tiles = stbte_get_tile(level->stbTileMap, x, y);
         for (int l = 0; l < level->maxLayers; ++l)
         {
            level->data[y * level->width * l + x] = tiles[l];
         }
      }
   }
}

static void dig(std::vector<uint8_t>& data, int width, int x0, int y0, int x1, int y1)
{
   if (x1 < x0)
   {
      int tmp = x1;
      x1 = x0;
      x0 = tmp;
   }

   if (y1 < y0)
   {
      int tmp = y1;
      y1 = y0;
      y0 = tmp;
   }

   for (int y = y0; y <= y1; ++y)
   {
      for (int x = x0; x <= x1; ++x)
      {
         data[y * width + x] = 50; // this is set to the floor
      }
   }
}

static void initObjectsFromDungeon(Level* level, std::vector<uint8_t>& data, int width, int height)
{
   level->width = width;
   level->height = height;
   level->data.resize(width * height * level->maxLayers);
   std::fill(level->data.begin(), level->data.end(), std::numeric_limits<uint16_t>::max());

   int x = 0;
   int y = 0;
   const auto area = level->width * level->height;

   for (auto i : data)
   {
      // The item isn't empty
      if (i != 0xFF)
      {
         for (int l = 0; l < level->maxLayers; ++l)
         {
            // Get the actor type if it exists as one option.
            bool isActorType = std::find(actors.begin(), actors.end(), i) != actors.end();

            if (isActorType && l == 3)
            {
               // Only set the actor to the 4th layer
               setTile(level, i, x, y, l);
            }
            else if (!isActorType && l == 0)
            {
               setTile(level, i, x, y, l);
            }
            else if (isActorType && l == 0)
            {
               setTile(level, 50, x, y, l);
            }
         }
      }

      ++x;
      if (x > (level->width - 1))
      {
         ++y;
         x = 0;
      }
   }

   const auto emptyTile = std::numeric_limits<uint16_t>::max();

   level->bsp->visit([&](BspTree& tree) -> bool {
      if (tree.isLeaf())
      {
         for (int y = tree.cellY; y < (tree.cellY + tree.cellHeight); ++y)
         {
            for (int x = tree.cellX; x < (tree.cellX + tree.cellWidth); ++x)
            {
               // Place the wall and floor tile adjacent to the wall
               if (getTileType(level, x, y - 1, 0) == emptyTile)
               {
                  if (y == tree.cellY)
                  {
                     setTile(level, 0, x, y - 1, 0);
                     setTile(level, 17, x, y, 0);
                     setTile(level, 33, x, y + 1, 1);
                  }
               }

               if (getTileType(level, x, y + 1, 0) == emptyTile)
               {
                  if (y == tree.cellY + tree.cellHeight - 1)
                  {
                     setTile(level, 8, x, y + 1, 1);
                  }
               }
            }
         }
      }
      else
      {
         // This could be a corridor connecting to rooms.
         for (int y = tree.cellY + 1; y < (tree.cellY + tree.cellHeight - 1); ++y)
         {
            for (int x = tree.cellX + 1; x < (tree.cellX + tree.cellWidth - 1); ++x)
            {
               auto c = getTileType(level, x, y, 0);

               if (isEmpty(level, x, y, 0) &&
                   (isEmpty(level, x, y - 1, 0) ||
                    getTileType(level, x, y - 1, 0) == 8) &&
                   isFloor(level, x, y + 1))
               {
                  setTile(level, 0, x, y - 1, 1);
                  setTile(level, 17, x, y, 0);
                  setTile(level, 33, x, y + 1, 0);
               }

               if (isEmpty(level, x, y, 0) &&
                   isFloor(level, x, y - 1) &&
                   isFloor(level, x, y + 1))
               {
                  setTile(level, 50, x, y, 0);
               }

               if (isEmpty(level, x, y, 0) &&
                   isFloor(level, x, y - 1))
               {
                  setTile(level, 8, x, y, 0);
               }

               if (isEmpty(level, x, y, 0) &&
                   isFloor(level, x, y - 1) &&
                   isFloor(level, x, y + 1))
               {
                  setTile(level, 50, x, y, 0);
               }
            }
         }
      }

      return false;
   });
}

struct Room
{
   int x0, y0, x1, y1;

   std::unique_ptr<Room> left;
   std::unique_ptr<Room> right;
};

void generateDungeon(Level* level)
{
   level->bsp->splitRecursive(15, 12, 12, 1.5f, 1.5f);

   std::vector<uint8_t> data;
   data.resize(bspW * bspH);
   std::fill(data.begin(), data.end(), 0xFF);

   static std::random_device rd;
   static std::mt19937 gen(rd());

   int roomNum = 0;
   int lastX = 0;
   int lastY = 0;

	int firstRoomX = 0;
	int firstRoomY = 0;

   Room monsterRoom;

   using Dist = std::uniform_int_distribution<>;
   level->bsp->visit([&](BspTree& tree) -> bool {
      if (tree.isLeaf())
      {
         Dist widthDis(6, tree.width - 2);
         Dist heightDis(6, tree.height - 2);

         auto w = widthDis(gen);
         auto h = heightDis(gen);
         auto x = Dist(tree.x + 1, tree.x + tree.width - w - 1)(gen);
         auto y = Dist(tree.y + 1, tree.y + tree.height - h - 1)(gen);

         tree.cellX = x;
         tree.cellY = y;
         tree.cellWidth = w;
         tree.cellHeight = h;

         dig(data, bspW, x, y, x + w - 1, y + h - 1);

         if (roomNum != 0)
         {
            dig(data, bspW, lastX, lastY, x + w / 2, lastY);
            dig(data, bspW, x + w / 2, lastY, x + w / 2, y + h / 2);

            if (roomNum == 1)
            {
               monsterRoom.x0 = x;
               monsterRoom.y0 = y;
               monsterRoom.x1 = x + w;
               monsterRoom.y1 = y + h;
            }
         }
         else
         {
            firstRoomX = x + w / 2;
            firstRoomY = y + h / 2;
         }

         lastX = x + w / 2;
         lastY = y + h / 2;
         ++roomNum;
      }

      return false;
   });

   // Put in objects

   // Grail
   data[lastY * bspW + lastX] = 0xFF;

   // Player
	data[firstRoomY * bspW + firstRoomX] = 234;

   auto mX = monsterRoom.x0 + (monsterRoom.x1 - monsterRoom.x0) / 2;
   auto mY = monsterRoom.y0 + (monsterRoom.y1 - monsterRoom.y0) / 2;
   data[mY * bspW + mX] = randomFromArray(babyMobs);

	initObjectsFromDungeon(level, data, bspW, bspH);
   convertLevelToStbTilemap(level);
}

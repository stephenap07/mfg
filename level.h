#ifndef LEVEL_H_
#define LEVEL_H_

#include "thirdparty/stb_tilemap_editor.h"

#include <list>

struct Player;
struct Actor;

struct Level
{
   std::size_t width = 30;
   std::size_t height = 20;
   int spacingX = 16;
   int spacingY = 16;
   int maxLayers = 5;
   std::unique_ptr<BspTree> bsp;
   stbte_tilemap* stbTileMap = nullptr;
   std::vector<uint16_t> data;
};

// Public interface
void initLevel(Level* level);

void writeLevel(Level* level, const std::string& filename);
void readLevel(Level* level, const std::string& filename);

uint16_t getTileType(Level* level, int x, int y, int l);
void setTile(Level* level, uint16_t value, int x, int y, int l);
bool isEmpty(Level* level, int x, int y, int l);
bool isActor(Level* level, int x, int y, int l);
bool isWall(Level* level, int x, int y);
bool isFloor(Level* level, int x, int y);

bool isMobType(std::size_t id);

void drawLevel(Level* level, int layer, int screenX, int screenY, int screenWidth, int screenHeight);

void generateDungeon(Level* level);

#endif

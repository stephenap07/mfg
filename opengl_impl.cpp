#define STB_IMAGE_IMPLEMENTATION
#include "thirdparty/stb_image.h"

#define STB_RECT_PACK_IMPLEMENTATION
#include "thirdparty/stb_rect_pack.h"

#define STB_TRUETYPE_IMPLEMENTATION
#include "thirdparty/stb_truetype.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "thirdparty/stb_image_write.h"

struct Texture
{
   int w;
   int h;
   int n;
   GLuint id;
};

struct SimpleVertex
{
   float x;
   float y;
   float r;
   float g;
   float b;
   float a;
};

struct TextureCoords
{
	GLfloat u, v;
};

struct TextVertexData
{
   float x, y, z;
   float u, v;
};

struct SpriteVertexData
{
   GLfloat x, y;
   GLfloat u, v;
	glm::mat4 model;
};

struct RectData
{
   int x0, y0, x1, y1;
   uint32_t color;
};

struct GLProgram
{
   GLuint program = 0;
   GLuint vertexShader = 0;
   GLuint fragmentShader = 0;

   GLuint uniOffset = 0;
   GLuint uniColor = 0;

   GLuint uniModel = 0;
   GLuint uniView = 0;
   GLuint uniProj = 0;

   GLuint vao = 0;
   GLuint vbo = 0;

	// New buffers
   GLuint vertexBuffer;
   GLuint textureCoordBuffer;
   GLuint matrixBuffer;
};

// Private methods
GLuint compileShader(GLenum shaderType, const GLchar* shaderSource);
Texture getTexture(const char* imageFile);

// error handling
void printGlError();

// Camera controls
void setView(int x, int y);
void resetView();

#define GLSL(src) (GLchar*)"#version 410 core\n" #src

// == Shaders ==
// clang-format off
constexpr GLchar* vertexSource = GLSL(
   in vec2 position;
   in vec2 texcoord;

   out vec3 Color;
   out vec2 Texcoord;

   uniform mat4 model;
   uniform mat4 proj;
   uniform mat4 view;

   uniform vec4 offset;
   uniform vec3 colorU;

   void main() {
      Color = colorU;
      Texcoord = offset.xy + texcoord * offset.zw;
      gl_Position = proj * view * model * vec4(position, 0.0, 1.0);
   }
);

constexpr GLchar* fragmentSource = GLSL(
   in vec3 Color;
   in vec2 Texcoord;

   out vec4 outColor;

   uniform sampler2D tex;

   void main() {
      vec4 textureColor = texture(tex, Texcoord);
      outColor = textureColor * vec4(Color, 1);
   }
);

constexpr GLchar* vertexInstancedSource = GLSL(
   in vec2 position;
   in vec2 texcoord;
	in mat4 modelMatrix;

   out vec2 Texcoord;

   uniform mat4 proj;
   uniform mat4 view;

   void main() {
      Texcoord = texcoord;
      gl_Position = proj * view * modelMatrix * vec4(position, 1.0, 1.0);
   }
);

constexpr GLchar* fragmentInstancedSource = GLSL(
   in vec2 Texcoord;

   out vec4 outColor;

   uniform sampler2D tex;

   void main() {
      vec4 textureColor = texture(tex, Texcoord);
		outColor = textureColor;
   }
);

constexpr GLchar* simpleVertexSource = GLSL(
   in vec3 position;
   in vec4 color;

   out vec4 Color;

   uniform mat4 model;
   uniform mat4 proj;
   uniform mat4 view;

   void main() {
      Color = color;
      gl_Position = proj * view * model * vec4(position, 1.0);
   }
);

constexpr GLchar* simpleFragmentSource = GLSL(
   in vec4 Color;
   out vec4 outColor;

   void main() {
      outColor = vec4(Color);
   }
);

constexpr GLchar* fontVertexSource = GLSL(
   in vec3 position;
   in vec2 texcoord;

   out vec2 Texcoord;

   uniform mat4 model;
   uniform mat4 proj;
   uniform mat4 view;

   void main() {
      Texcoord = texcoord;
      gl_Position = proj * view * model * vec4(position, 1.0);
   }
);

constexpr GLchar* fontFragmentSource = GLSL(
   in vec2 Texcoord;

   out vec4 outColor;

   uniform sampler2D tex;

   void main() {
      vec4 textureColor = texture(tex, Texcoord);
      outColor = vec4(0, 0, 0, 1) * vec4(textureColor.r, textureColor.r, textureColor.r, textureColor.r);
   }
);

// clang-format on

// Vertex data
const GLfloat vertices[] = {
   //  Position  Color             Texcoords
   -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, // Top-left
   -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, // Bottom-left
    1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, // Bottom-right
    1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, // Bottom-right
    1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, // Top-right
   -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, // Top-left
};

// OpenGL state
GLProgram spriteProgram;
GLProgram spriteInstancedProgram;
GLProgram simpleProgram;
GLProgram fontProgram;

// Camera controls
int ViewX = 0;
int ViewY = 0;

Texture spriteTexture;
Texture fontTexture;

static const int atlasWidth = 512;
static const int atlasHeight = 512;
static const int lineHeight = 14;
static const uint32_t firstChar = ' ';
static const uint32_t charCount = '~' - ' ';
static std::unique_ptr<stbtt_packedchar[]> packedChars;
static stbtt_bakedchar cdata[96];

static void setRenderView(GLuint uniView)
{
   glm::mat4 view = glm::lookAt(glm::vec3(0, 0, 1),  // eye
                                glm::vec3(0, 0, 0),  // center
                                glm::vec3(0, 1, 0)); // up
   auto newMat = view * glm::translate(glm::mat4(), -glm::vec3(ViewX, ViewY, 0));
   glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(newMat));
}

static void setRenderProjection(GLuint uniProj)
{
   glm::mat4 proj = glm::ortho(0.0f, ScaledWidth, ScaledHeight, 0.0f);
   glUniformMatrix4fv(uniProj, 1, GL_FALSE, glm::value_ptr(proj));
}

void drawSprite(int x0, int y0, int x1, int y1, int u, int v, int s, int t)
{
   Texture& tex = spriteTexture;

   // Draw the sprite
   glUseProgram(spriteProgram.program);
   glActiveTexture(GL_TEXTURE0);
   glBindTexture(GL_TEXTURE_2D, tex.id);

   // Set up sprite cut out
   if (s == 0)
   {
      s = tex.w;
   }
   if (t == 0)
   {
      t = tex.h;
   }

   glUniform4f(spriteProgram.uniOffset,
               (float)u / (GLfloat)tex.w,
               (float)v / (GLfloat)tex.h,
               (float)s / (GLfloat)tex.w,
               (float)t / (GLfloat)tex.h);

   // Set up model
   float width = x1 - x0;
   float height = y1 - y0;

   glm::vec2 charSize = glm::vec2(width, height);

   glm::mat4 model
      = glm::translate(glm::mat4(), glm::vec3(x0, y0, 0.f))
      * glm::translate(glm::mat4(), glm::vec3(charSize / 2.f, 0.f))
      * glm::scale(glm::mat4(), glm::vec3(charSize / 2.f, 1.f));
   
   glUniformMatrix4fv(spriteProgram.uniModel, 1, GL_FALSE, glm::value_ptr(model));

   // Set up color
   glUniform3f(spriteProgram.uniColor, 1.f, 1.f, 1.f);

	setRenderView(spriteProgram.uniView);
   setRenderProjection(spriteProgram.uniProj);

   // Draw
   glBindVertexArray(spriteProgram.vao);
   glDrawArrays(GL_TRIANGLES, 0, 6);
}

std::vector<SpriteVertexData> gSpriteVertexData;
std::vector<RectData> gRectData;

void beginSpriteInstance()
{
   gSpriteVertexData.clear();
   gRectData.clear();
}

void drawSpriteInstanced(int x0, int y0, int x1, int y1, int u, int v, int s, int t, glm::mat4 transform)
{
   Texture& tex = spriteTexture;

   // Set up sprite cut out
   if (s == 0)
   {
      s = tex.w;
   }

   if (t == 0)
   {
      t = tex.h;
   }

   glm::vec4 offset((float)u / (GLfloat)tex.w,
                    (float)v / (GLfloat)tex.h,
                    (float)s / (GLfloat)tex.w,
                    (float)t / (GLfloat)tex.h);

   // Set up model
   float width = x1 - x0;
   float height = y1 - y0;

   glm::vec2 charSize = glm::vec2(width, height);

   glm::mat4 model
      = glm::translate(glm::mat4(), glm::vec3(x0, y0, 0.f))
      * transform
      * glm::translate(glm::mat4(), glm::vec3(charSize / 2.f, 0.f))
      * glm::scale(glm::mat4(), glm::vec3(charSize / 2.f, 1.f));

   gSpriteVertexData.push_back({ -1.0f, -1.0f, offset.x, offset.y, model }); // top left
   gSpriteVertexData.push_back({ -1.0f, 1.0f, offset.x, offset.y + offset.w, model }); // bottom left
   gSpriteVertexData.push_back({ 1.0f, 1.0f, offset.x + offset.z, offset.y + offset.w, model }); // bottom right
   gSpriteVertexData.push_back({ 1.0f, 1.0f, offset.x + offset.z, offset.y + offset.w, model }); // bottom right
   gSpriteVertexData.push_back({ 1.0f, -1.0f, offset.x + offset.z, offset.y, model }); // top right
   gSpriteVertexData.push_back({ -1.0f, -1.0f, offset.x, offset.y, model }); // top left
}

void endSpriteInstance()
{
   Texture& tex = spriteTexture;

	glBindBuffer(GL_ARRAY_BUFFER, spriteInstancedProgram.vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(SpriteVertexData) * gSpriteVertexData.size(), &gSpriteVertexData[0], GL_STREAM_DRAW);

   glUseProgram(spriteInstancedProgram.program);
   glActiveTexture(GL_TEXTURE0);
   glBindTexture(GL_TEXTURE_2D, tex.id);

	// Uniforms
	setRenderView(spriteInstancedProgram.uniView);
   setRenderProjection(spriteInstancedProgram.uniProj);

   // Draw
   glBindVertexArray(spriteInstancedProgram.vao);
   glDrawArrays(GL_TRIANGLES, 0, gSpriteVertexData.size());

   printGlError();

   for (auto& r : gRectData)
   {
      drawRect(r.x0, r.y0, r.x1, r.y1, r.color);
   }
}

void drawRect(int x0, int y0, int x1, int y1, uint32_t color)
{
   uint8_t ai = (color & 0xFF000000) >> 24;
   uint8_t ri = (color & 0x00FF0000) >> 16;
   uint8_t gi = (color & 0x0000FF00) >> 8;
   uint8_t bi = (color & 0x000000FF);

   float r = (float)ri / 255.f;
   float g = (float)gi / 255.f;
   float b = (float)bi / 255.f;
   float a = (float)ai / 255.f;

   SimpleVertex vertices[6] = {
      { -1.0f, -1.0f, r, g, b, a }, // Top-left
      { -1.0f, 1.0f, r, g, b, a }, // Bottom-left
      { 1.0f, 1.0f, r, g, b, a }, // Bottom-right
      { 1.0f, 1.0f, r, g, b, a }, // Bottom-right
      { 1.0f, -1.0f, r, g, b, a }, // Top-right
      { -1.0f, -1.0f, r, g, b, a }, // Top-left
   };

   glBindBuffer(GL_ARRAY_BUFFER, simpleProgram.vbo);
   glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STREAM_DRAW);

   // Uniforms
   auto charSize = glm::vec2((float)x1 - x0, (float)y1 - y0);

   auto model = glm::translate(glm::mat4(), glm::vec3(x0, y0, 0.f))
      * glm::translate(glm::mat4(), glm::vec3(charSize / 2.f, 0.f))
      * glm::scale(glm::mat4(), glm::vec3(charSize / 2.f, 1.f));

   glUseProgram(simpleProgram.program);
   glUniformMatrix4fv(simpleProgram.uniModel, 1, GL_FALSE, glm::value_ptr(model));

	setRenderView(simpleProgram.uniView);
   setRenderProjection(simpleProgram.uniProj);

   glBindVertexArray(simpleProgram.vao);
   glDrawArrays(GL_TRIANGLES, 0, 6);
}

void drawRectDeferred(int x0, int y0, int x1, int y1, uint32_t color)
{
   gRectData.push_back(RectData{x0, y0, x1, y1, color});
}

void drawText(int x, int y, std::string text)
{
   // Draw the text
   float offsetX = 0;
   float offsetY = 0;

   std::vector<TextVertexData> textData;
   textData.reserve(text.size() * 6);

   for (auto c : text)
   {
      stbtt_aligned_quad quad;
      stbtt_GetPackedQuad(packedChars.get(), atlasWidth, atlasHeight, c - firstChar, &offsetX, &offsetY, &quad, 1);

      float minX = quad.x0;
      float maxX = quad.x1;
      float minY = quad.y0;
      float maxY = quad.y1;

      // x, y, z, u, v
      textData.push_back({ minX, minY, 1.f, quad.s0, quad.t0 }); // top left
      textData.push_back({ minX, maxY, 1.f, quad.s0, quad.t1 }); // bottom left
      textData.push_back({ maxX, maxY, 1.f, quad.s1, quad.t1 }); // bottom right
      textData.push_back({ maxX, maxY, 1.f, quad.s1, quad.t1 }); // bottom right
      textData.push_back({ maxX, minY, 1.f, quad.s1, quad.t0 }); // top right
      textData.push_back({ minX, minY, 1.f, quad.s0, quad.t0 }); // top left
   }

   glBindBuffer(GL_ARRAY_BUFFER, fontProgram.vbo);
   glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 5 * textData.size(), &textData[0], GL_STREAM_DRAW);

   glUseProgram(fontProgram.program);
   glActiveTexture(GL_TEXTURE0);
   glBindTexture(GL_TEXTURE_2D, fontTexture.id);

   // Uniforms
   auto model = glm::mat4();
   model = glm::translate(model, glm::vec3(x, y, 0));
   glUniformMatrix4fv(fontProgram.uniModel, 1, GL_FALSE, glm::value_ptr(model));

   // Set up view
	setRenderView(fontProgram.uniView);
   setRenderProjection(fontProgram.uniProj);

   glBindVertexArray(fontProgram.vao);
   glDrawArrays(GL_TRIANGLES, 0, textData.size());
}

void drawBorderedRect(int x0, int y0, int x1, int y1, int borderThickness, uint32_t rectColor, uint32_t borderColor)
{
   drawRect(x0, y0, x1, y1, rectColor);

   drawRect(x0, y0, x1 - borderThickness, y0 + borderThickness, borderColor);
   drawRect(x1 - borderThickness, y0, x1, y1 - borderThickness, borderColor);
   drawRect(x0 + borderThickness, y1 - borderThickness, x1, y1, borderColor);
   drawRect(x0, y0 + borderThickness, x0 + borderThickness, y1, borderColor);
}

void setView(int x, int y)
{
	ViewX = x;
	ViewY = y;
}

void resetView()
{
   ViewX = 0;
   ViewY = 0;
}

void initSprite()
{
   // clear errors
   glGenVertexArrays(1, &spriteProgram.vao);
   glBindVertexArray(spriteProgram.vao);

   glGenBuffers(1, &spriteProgram.vbo);
   glBindBuffer(GL_ARRAY_BUFFER, spriteProgram.vbo);
   glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

   spriteProgram.program = glCreateProgram();
   spriteProgram.vertexShader = compileShader(GL_VERTEX_SHADER, vertexSource);
   spriteProgram.fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentSource);
   glAttachShader(spriteProgram.program, spriteProgram.vertexShader);
   glAttachShader(spriteProgram.program, spriteProgram.fragmentShader);
   glLinkProgram(spriteProgram.program);

   glUseProgram(spriteProgram.program);

   GLint posAttrib = glGetAttribLocation(spriteProgram.program, "position");
   glEnableVertexAttribArray(posAttrib);
   glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), 0);

   GLint texAttrib = glGetAttribLocation(spriteProgram.program, "texcoord");
   glEnableVertexAttribArray(texAttrib);
   glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(5 * sizeof(GLfloat)));

   // Set up the texture
   spriteTexture = getTexture("assets/sprite1.png");

   // Set up uniform ids
   spriteProgram.uniOffset = glGetUniformLocation(spriteProgram.program, "offset");
   spriteProgram.uniModel = glGetUniformLocation(spriteProgram.program, "model");
   spriteProgram.uniView = glGetUniformLocation(spriteProgram.program, "view");
   spriteProgram.uniProj = glGetUniformLocation(spriteProgram.program, "proj");
   spriteProgram.uniColor = glGetUniformLocation(spriteProgram.program, "colorU");

   printGlError();
}

void initSpriteInstanced()
{
   glGenVertexArrays(1, &spriteInstancedProgram.vao);
   glBindVertexArray(spriteInstancedProgram.vao);

   spriteInstancedProgram.program = glCreateProgram();
   spriteInstancedProgram.vertexShader = compileShader(GL_VERTEX_SHADER, vertexInstancedSource);
   spriteInstancedProgram.fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentInstancedSource);
   glAttachShader(spriteInstancedProgram.program, spriteInstancedProgram.vertexShader);
   glAttachShader(spriteInstancedProgram.program, spriteInstancedProgram.fragmentShader);
   glLinkProgram(spriteInstancedProgram.program);
   glUseProgram(spriteInstancedProgram.program);

   // Vertex data shared by all sprites.

	static const GLfloat vertexData[] = {
		//  Position
		-1.0f, -1.0f, // Top-left
		-1.0f,  1.0f, // Bottom-left
		 1.0f,  1.0f, // Bottom-right
		 1.0f,  1.0f, // Bottom-right
		 1.0f, -1.0f, // Top-right
		-1.0f, -1.0f, // Top-left
	};

   glGenBuffers(1, &spriteInstancedProgram.vbo);
   glBindBuffer(GL_ARRAY_BUFFER, spriteInstancedProgram.vbo);

   GLint posAttrib = glGetAttribLocation(spriteInstancedProgram.program, "position");
   glEnableVertexAttribArray(posAttrib);
   glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertexData), nullptr);

   GLint texAttrib = glGetAttribLocation(spriteInstancedProgram.program, "texcoord");
   glEnableVertexAttribArray(texAttrib);
   glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertexData), (void*)(2 * sizeof(GLfloat)));

   GLint matrixAttrib = glGetAttribLocation(spriteInstancedProgram.program, "modelMatrix");
   for (int i = 0; i < 4; ++i)
   {
      glVertexAttribPointer(matrixAttrib + i, 4, GL_FLOAT, GL_FALSE, sizeof(SpriteVertexData), (void*)(4 * sizeof(GLfloat) + sizeof(glm::vec4) * i));
      glEnableVertexAttribArray(matrixAttrib + i);
   }

   // Set up the texture if not already set up
   if (spriteTexture.id == 0)
   {
      spriteTexture = getTexture("assets/sprite1.png");
   }

   // Set up uniform ids
   spriteInstancedProgram.uniView = glGetUniformLocation(spriteInstancedProgram.program, "view");
   spriteInstancedProgram.uniProj = glGetUniformLocation(spriteInstancedProgram.program, "proj");

   printGlError();
}

void initSimple()
{
   simpleProgram.program = glCreateProgram();

   simpleProgram.vertexShader = compileShader(GL_VERTEX_SHADER, simpleVertexSource);
   simpleProgram.fragmentShader = compileShader(GL_FRAGMENT_SHADER, simpleFragmentSource);

   glAttachShader(simpleProgram.program, simpleProgram.vertexShader);
   glAttachShader(simpleProgram.program, simpleProgram.fragmentShader);
   glLinkProgram(simpleProgram.program);

   // Specify the layout of the vertex data in the shader program
   glUseProgram(simpleProgram.program);

   glGenVertexArrays(1, &simpleProgram.vao);
   glBindVertexArray(simpleProgram.vao);

   // Create a Vertex Buffer Object and copy the vertex data to it
   glGenBuffers(1, &simpleProgram.vbo);
   glBindBuffer(GL_ARRAY_BUFFER, simpleProgram.vbo);

   GLint posAttrib = glGetAttribLocation(simpleProgram.program, "position");
   glEnableVertexAttribArray(posAttrib);
   glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);

   GLint colorAttrib = glGetAttribLocation(simpleProgram.program, "color");
   glEnableVertexAttribArray(colorAttrib);
   glVertexAttribPointer(colorAttrib, 4, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

   simpleProgram.uniModel = glGetUniformLocation(simpleProgram.program, "model");
   simpleProgram.uniView = glGetUniformLocation(simpleProgram.program, "view");
   simpleProgram.uniProj = glGetUniformLocation(simpleProgram.program, "proj");
}

void initFont()
{
   fontProgram.program = glCreateProgram();
   fontProgram.vertexShader = compileShader(GL_VERTEX_SHADER, fontVertexSource);
   fontProgram.fragmentShader = compileShader(GL_FRAGMENT_SHADER, fontFragmentSource);
   glAttachShader(fontProgram.program, fontProgram.vertexShader);
   glAttachShader(fontProgram.program, fontProgram.fragmentShader);
   glLinkProgram(fontProgram.program);

   // Specify the layout of the vertex data in the shader program
   glUseProgram(fontProgram.program);

   glGenVertexArrays(1, &fontProgram.vao);
   glBindVertexArray(fontProgram.vao);

   // Create a Vertex Buffer Object and copy the vertex data to it
   glGenBuffers(1, &fontProgram.vbo);
   glBindBuffer(GL_ARRAY_BUFFER, fontProgram.vbo);

   GLint posAttrib = glGetAttribLocation(fontProgram.program, "position");
   glEnableVertexAttribArray(posAttrib);
   glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);

   GLint texAttrib = glGetAttribLocation(fontProgram.program, "texcoord");
   glEnableVertexAttribArray(texAttrib);
   glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));

   fontProgram.uniModel = glGetUniformLocation(fontProgram.program, "model");
   fontProgram.uniView = glGetUniformLocation(fontProgram.program, "view");
   fontProgram.uniProj = glGetUniformLocation(fontProgram.program, "proj");

   std::size_t size = 0;
   auto fileName = "assets/munro/Munro.ttf";
   auto theFile = fopen(fileName, "rb");

   if (!theFile)
   {
	   SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error loading asset", (std::string("Missing file ") + std::string(fileName)).c_str(), nullptr);
	   exit(1);
   }

   fseek(theFile, 0, SEEK_END);
   size = ftell(theFile);
   fseek(theFile, 0, SEEK_SET);

   unsigned char* fontBuffer = (unsigned char*)malloc(size);
   fread(fontBuffer, 1, size, theFile);

   stbtt_fontinfo info;
   if (!stbtt_InitFont(&info, fontBuffer, 0))
   {
      printf("failed\n");
   }

   auto atlasBuffer = std::make_unique<uint8_t[]>(atlasWidth * atlasHeight);
   packedChars = std::make_unique<stbtt_packedchar[]>(charCount);

   stbtt_pack_context context;
   if (!stbtt_PackBegin(&context, atlasBuffer.get(), atlasWidth, atlasHeight, 0, 1, nullptr))
   {
      fprintf(stderr, "Error initializing font\n");
   }

   stbtt_PackSetOversampling(&context, 2, 2);
   if (!stbtt_PackFontRange(&context, fontBuffer, 0, lineHeight, firstChar,
                            charCount, packedChars.get()))
   {
      fprintf(stderr, "Error initializing font\n");
   }

   stbtt_PackEnd(&context);

   free(fontBuffer);

   fontTexture = Texture{ atlasWidth, atlasHeight, 1, 0 };

   glGenTextures(1, &fontTexture.id);
   glBindTexture(GL_TEXTURE_2D, fontTexture.id);
   glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, atlasWidth, atlasHeight, 0, GL_RED, GL_UNSIGNED_BYTE, atlasBuffer.get());

   glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
   glGenerateMipmap(GL_TEXTURE_2D);

   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}

GLuint compileShader(GLenum shaderType, const GLchar* shaderSource)
{
   GLuint shader = glCreateShader(shaderType);
   glShaderSource(shader, 1, &shaderSource, nullptr);
   glCompileShader(shader);
   GLint success = 0;
   glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
   if (success == GL_FALSE)
   {
      GLint logSize = 0;
      glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
      std::vector<GLchar> errorLog(logSize);
      glGetShaderInfoLog(shader, logSize, &logSize, &errorLog[0]);
      if (!errorLog.empty())
      {
         fprintf(stderr, "Error with shader %s\n", &errorLog[0]);
      }
   }
   return shader;
}

Texture getTexture(const char* imageFile)
{
   Texture ret;

   unsigned char *imgData = stbi_load(imageFile, &ret.w, &ret.h, &ret.n, 0);

   if (!imgData)
   {
      fprintf(stderr, "Failed to load image %s\n", imageFile);
   }

   GLuint id;
   glGenTextures(1, &id);
   ret.id = id;
   glBindTexture(GL_TEXTURE_2D, id);

   GLint mode = GL_RGB;
   if (ret.n == 4)
   {
      mode = GL_RGBA;
   }

   glTexImage2D(GL_TEXTURE_2D, 0, mode, ret.w, ret.h, 0, mode, GL_UNSIGNED_BYTE, imgData);

   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

   stbi_image_free(imgData);

   return ret;
}

void printGlError()
{
   auto err = glGetError();
   if (err == GL_OUT_OF_MEMORY)
   {
      printf("Ran out of GL memory\n");
   }
   if (err == GL_INVALID_ENUM)
   {
      printf("GL invalid enum\n");
   }
   if (err == GL_INVALID_VALUE)
   {
      printf("GL invalid value\n");
   }
   if (err == GL_INVALID_FRAMEBUFFER_OPERATION)
   {
      printf("GL invalid framebuffer operation\n");
   }
}

int getViewX()
{
   return ViewX;
}

int getViewY()
{
   return ViewY;
}

void initOpenGL()
{
   glewExperimental = GL_TRUE;
   glewInit();

   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   // Clear color set to white
   glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
   initSprite();
   initSpriteInstanced();
   initSimple();
   initFont();
}

#include "bsp.h"

#include <random>

BspTree::BspTree(int x, int y, int w, int h, BspTree* parent)
   : x(x)
   , y(y)
   , width(w)
   , height(h)
   , cellX(x)
   , cellY(y)
   , cellWidth(w)
   , cellHeight(h)
   , index(0)
   , parent(parent)
   , front(nullptr)
   , back(nullptr)
{
}

void BspTree::splitRecursive(int numLevels, int minW, int minH, float wRatio, float hRatio)
{
   if (numLevels == 0 || (width < 2 * minW && height < 2 * minH))
   {
      return;
   }

   static std::random_device rd;
   static std::mt19937 gen(rd());
   std::uniform_int_distribution<> orientDis(0, 1);
   std::uniform_int_distribution<> widthDis(minW, std::max(width - minW, minW));
   std::uniform_int_distribution<> heightDis(minH, std::max(height - minH, minH));

   bool horizontal;
   if (height < 2 * minH || width > height * wRatio)
   {
      horizontal = false;
   }
   else if (width < 2 * minW || height > width * hRatio)
   {
      horizontal = true;
   }
   else
   {
      horizontal = orientDis(gen);
   }

   split(horizontal, horizontal ? heightDis(gen) : widthDis(gen));
   front->splitRecursive(numLevels - 1, minW, minH, wRatio, hRatio);
   back->splitRecursive(numLevels - 1, minW, minH, wRatio, hRatio);
}

void BspTree::split(bool horizontal, int value)
{
   if (horizontal)
   {
      front = std::make_unique<BspTree>(x, y, width, value, this);
      back = std::make_unique<BspTree>(x, y + value, width, height - value, this);
   }
   else // vertical
   {
      front = std::make_unique<BspTree>(x, y, value, height, this);
      back = std::make_unique<BspTree>(x + value, y, width - value, height, this);
   }
   front->index = index + 1;
   back->index = index + 2;
}

void BspTree::visit(std::function<bool(BspTree&)> visitor)
{
	// Processes the leaves first
   std::deque<BspTree*> stack1;
   std::deque<BspTree*> stack2;

   stack1.push_front(this);

   while (!stack1.empty())
   {
      BspTree* node = stack1.front();
		stack1.pop_front();
      stack2.push_front(node);
      if (node->front)
         stack1.push_front(node->front.get());
      if (node->back)
         stack1.push_front(node->back.get());
   }

   while (!stack2.empty())
   {
      BspTree* node = stack2.front();
      stack2.pop_front();
      if (visitor(*node))
      {
         return;
      }
   }
}

BspTree* BspTree::findNode(int ax, int ay)
{
   BspTree* found = nullptr;

   // Find the smallest node containing ax and ay.
   visit([&](BspTree& t) -> bool {
      if (ax >= t.cellX &&
          ay >= t.cellY &&
          ax < (t.cellX + t.cellWidth) &&
          ay < (t.cellY + t.cellHeight))
      {
         found = &t;
         return true;
      }

      return false;
   });

   return found;
}

bool BspTree::isLeaf() const
{
   return !front && !back;
}


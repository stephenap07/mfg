// Holee Grail
// A prototype by Stephen
// For 'My First Game Jam Winter 2018'

#define GLEW_STATIC
#include <GL/glew.h>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdio.h>

#include <memory>
#include <string>
#include <vector>

// Data types
struct Clock
{
   uint32_t lastTickTime = 0;
   float delta = 0;

   void tick()
   {
      uint32_t tickTime = SDL_GetTicks();
      delta = (float)(tickTime - lastTickTime) * 0.001f;
      lastTickTime = tickTime;
   }
};

static SDL_Window* window = nullptr;
static SDL_GLContext context = nullptr;
static SDL_Event event;

// == Variables ==

constexpr static int SpacingX = 16;
constexpr static int SpacingY = 16;

constexpr static float ScreenWidth = 1024;
constexpr static float ScreenHeight = 720;

constexpr static float ScaledWidth = ScreenWidth / 2.f;
constexpr static float ScaledHeight = ScreenHeight / 2.f;

constexpr float RatioW = ScreenWidth / ScaledWidth;
constexpr float RatioH = ScreenHeight / ScaledHeight;

constexpr int FramesPerSecond = 60;

// Draw primitives
void beginSpriteInstance();
void endSpriteInstance();
void drawSprite(int x0, int y0, int x1, int y1, int u = 0, int v = 0, int s = 0, int t = 0);
void drawSpriteInstanced(int x0, int y0, int x1, int y1, int u, int v, int s = 0, int t = 0, glm::mat4 transform = glm::mat4());
void drawRect(int x0, int y0, int x1, int y1, uint32_t color);
void drawRectDeferred(int x0, int y0, int x1, int y1, uint32_t color);
void drawText(int x, int y, std::string text);
void drawBorderedRect(int x0, int y0, int x1, int y1, int borderThickness, uint32_t rectColor, uint32_t borderColor);
int getViewX();
int getViewY();

// Camera controls
void setView(int x, int y);
void resetView();

// Game code accessed from system
void initGame();
void gameLoop();

#include "game.cpp"

#include "actor.cpp"
#include "weapon.cpp"
#include "mortal.cpp"
#include "editor.cpp"
#include "level.cpp"
#include "player.cpp"
#include "bsp.cpp"

// Some private initialization functions
void initOpenGL();

#include "opengl_impl.cpp"

static void initWindow()
{
   // == SDL ==

   SDL_Init(SDL_INIT_VIDEO);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

   // Enable vsync
   SDL_GL_SetSwapInterval(1);
   window = SDL_CreateWindow("Holee Grail",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             ScreenWidth,
                             ScreenHeight,
                             SDL_WINDOW_OPENGL);
   if (!window)
   {
      fprintf(stderr, "Error: Unable to make window. SDL Error: %s", SDL_GetError());
   }

   context = SDL_GL_CreateContext(window);

   if (!context)
   {
      fprintf(stderr, "Error: Unable to create context. SDL Error: %s", SDL_GetError());
   }

   if (SDL_GL_SetSwapInterval(1) < 0)
   {
      fprintf(stderr, "Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
   }
}

#ifdef _WIN32
int WINAPI WinMain(HINSTANCE hInstance,
				   HINSTANCE hPrevInstance,
				   LPSTR lpCmdLine,
				   int nCmdShow)
#else
int main(int argc, char** argv)
#endif
{
   // Lower level system initialization
   initWindow();
   initOpenGL();

   // Game related initialization
   initGame();
   gameLoop();

   return 0;
}

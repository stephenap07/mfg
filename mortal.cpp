#include "mortal.h"

bool isAlive(Mortal* mortal)
{
   return mortal->hp > 0;
}

void tickMortal(Mortal* mortal, Game* game)
{
   auto actor = get(&game->actorRegister, mortal->actorId);
   if (mortal->hp <= 0)
   {
      setNextAction(actor, ActionType::Die);
   }
}

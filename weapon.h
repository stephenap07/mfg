#ifndef WEAPON_H_
#define WEAPON_H_

#include <array>
#include <memory>
#include <vector>

struct Hand
{
   int id = 0;
   int startX = 8;
   int startY = 8;
   int radius = 4;
};

enum WeaponType : uint8_t {
   None,
   Sword
};

struct Weapon
{
   float startAngle = -45.f;
   float stopAngle = 20.f;;
   float hitSpeed = 10.f;
};

struct WeaponHolder
{
   std::size_t actorId;
   WeaponType weaponType = WeaponType::None;
   Hand hand;

   float angle = 0.f;
   bool startAttack = false;
   bool attackInProgress = false;
};

using WeaponHolderRegister = ComponentRegister<WeaponHolder>;

void tickWeapon(Game* game);
void drawWeaponHolders(Game* game);

#endif

#define STB_TILEMAP_EDITOR_IMPLEMENTATION

#define STBTE_MAX_PROPERTIES     10
#define STBTE_MAX_TILEMAP_X 300
#define STBTE_MAX_TILEMAP_Y 300

void STBTE_DRAW_RECT(int x0, int y0, int x1, int y1, uint32_t color);
// this must draw a filled rectangle (exclusive on right/bottom)
// color = (r<<16)|(g<<8)|(b)

void STBTE_DRAW_TILE(int x0, int y0, unsigned short id, int highlight, float* data);
// this draws the tile image identified by 'id' in one of several
// highlight modes (see STBTE_drawmode_* in the header section);
// if 'data' is NULL, it's drawing the tile in the palette; if 'data'
// is not NULL, it's drawing a tile on the map, and that is the data
// associated with that map tile
                                                                      
#include "thirdparty/stb_tilemap_editor.h"


void STBTE_DRAW_RECT(int x0, int y0, int x1, int y1, uint32_t color)
{
   // Make the color have full alpha value
   drawRect(x0, y0, x1, y1, 0xFF000000 | color);
}

void STBTE_DRAW_TILE(int x0, int y0, unsigned short id, int highlight, float* data)
{
   if (highlight == -1)
   {
      // deemphesize
      //spr.color = Color{ 128, 128, 128 };
   }
   else if (highlight == 1)
   {
      //spr.color = Color{ 255, 255, 0 };
   }

   drawSprite(x0, y0, x0 + SpacingX, y0 + SpacingY,
              (id % SpacingX) * SpacingX, (id / SpacingY) * SpacingY,
              SpacingX, SpacingY);
}

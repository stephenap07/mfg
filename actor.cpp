#include "actor.h"

int solveMove(int* dir, uint64_t* timer, float *pos, int *tilePos)
{
   const int maxTimer = 4;

   if (*dir != 0 && *timer == 0)
   {
      *timer = maxTimer;
   }

   if (*timer > 0)
   {
      *pos += ((float)SpacingX / maxTimer) * static_cast<float>(*dir);
      *timer -= 1;
      if (*timer == 0)
      {
         *tilePos += *dir;
         *dir = 0;
         *pos = 0;

         if (*dir > 0)
         {
            return 1;
         }
         else
         {
            return -1;
         }
      }
   }

   return 0;
}

static bool canMoveTo(Game* game, int x, int y)
{
   // Super simple collision checking
   if (x != 0 || y != 0)
   {
      if (x < game->level.width && y < game->level.height)
      {
         auto targetId = getActorFromLevel(game, x, y);

         // Check first layer for collisions only for now.
         if (isWall(&game->level, x, y) ||
             isValidId(&game->actorRegister, targetId))
         {
            return false;
         }
      }
   }

   return true;
}

void tickMob(Actor* actor, Game* game)
{
   ActorState* state = &actor->actorState;

   if (actor->nextAction &&
       actor->nextAction->type == ActionType::Die)
   {
      return;
   }

   if (game->state == NewTurn)
   {
      auto player = get(&game->actorRegister, game->actorRegister.playerId);

      if (!player)
      {
         // player is possibly dead or something else happened
         return;
      }

      int dX = player->tileX - actor->tileX;
      int dY = player->tileY - actor->tileY;

      int stepX = dX > 0 ? 1 : -1;
      int stepY = dY > 0 ? 1 : -1;

      float distance = std::sqrt(dX * dX + dY * dY);

      if (distance >= 2)
      {
         dX = (int)(std::round(dX / distance));
         dY = (int)(std::round(dY / distance));

         if (canMoveTo(game, actor->tileX + dX, actor->tileY + dY))
         {
            actor->actorState.xDir = dX;
            actor->actorState.yDir = dY;
         }
         else if (canMoveTo(game, actor->tileX + stepX, actor->tileY))
         {
            actor->actorState.xDir = stepX;
         }
         else if (canMoveTo(game, actor->tileX, actor->tileY + stepY))
         {
            actor->actorState.yDir = stepY;
         }
      }
      else
      {
         actorAttack(actor, player->id);
      }
   }

   solveMove(&state->xDir, &state->timerX, &state->x, &actor->tileX);
   solveMove(&state->yDir, &state->timerY, &state->y, &actor->tileY);

   actor->x = actor->tileX * SpacingX + state->x;
   actor->y = actor->tileY * SpacingY + state->y;
}

void doAction(Action* action, Game* game)
{
   auto actor = get(&game->actorRegister, action->target);

   switch (action->type)
   {
   case ActionType::Attack:
      if (isValidId(actor->mortalId))
      {
         auto mortal = get(&game->mortalRegister, action->target);
         if (isAlive(mortal))
         {
            --mortal->hp;
         }
      }
      break;
   case ActionType::Die:
      break;
   default:
      break;
   };
}

void drawActor(Actor* actor, ActorState* state)
{
   drawSpriteInstanced(actor->x, actor->y, actor->x + SpacingX, actor->y + SpacingY, (actor->type % SpacingX) * SpacingX, (actor->type / SpacingY) * SpacingY, SpacingX, SpacingY);
   if (actor->id == 234)
   {
      drawRectDeferred(actor->x, actor->y, actor->x + SpacingX, actor->y + SpacingY, 0xBBA9A9A9);
   }
}

void setNextAction(Actor* actor, ActionType actionType)
{
   actor->nextAction = std::move(std::make_unique<Action>());
   actor->nextAction->type = actionType;
   actor->nextAction->owner = actor->id;
   actor->nextAction->target = 0;
}

void actorAttack(Actor* actor, uint16_t targetActorId)
{
   actor->nextAction = std::move(std::make_unique<Action>());
   actor->nextAction->type = ActionType::Attack;
   actor->nextAction->owner = actor->id;
   actor->nextAction->target = targetActorId;
}


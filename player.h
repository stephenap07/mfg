#ifndef PLAYER_H_
#define PLAYER_H_

#include "actor.h"

struct Game;

struct Player
{
   Actor* actor;
   Attributes attributes;
   std::size_t actorId = 0;
   std::size_t weaponId = 0;
   std::size_t mortalId = 0;
};

void initPlayer(Player* player);
void handlePlayerEvents(Actor* player, ActorState* actorState, Game* game, const Uint8* state);
bool tickPlayer(Actor* player, Game* game);

#endif

#ifndef MORTAL_H_
#define MORTAL_H_

struct Mortal
{
   std::size_t actorId = 0;
   int maxHp = 30;
   int hp = maxHp;
};

using MortalRegister = ComponentRegister<Mortal>;

bool isAlive(Mortal* mortal);

void tickMortal(Mortal* mortal, Game* game);

#endif
